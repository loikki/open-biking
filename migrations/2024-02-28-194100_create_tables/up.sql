CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name varchar(255) NOT NULL,
  UNIQUE(name),
);

CREATE INDEX idx_name ON users(name);

CREATE TABLE training_data (
  id SERIAL PRIMARY KEY,
  datetime DATETIME NOT NULL,
  effort SMALLINT UNSIGNED NOT NULL,
  distance FLOAT  NOT NULL,
  time BIGINT UNSIGNED NOT NULL,
  average_cadence FLOAT NOT NULL,
  average_resistance FLOAT NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL REFERENCES users(id)
);

CREATE TABLE training_settings (
  id SERIAL PRIMARY KEY,
  effort SMALLINT UNSIGNED NOT NULL,
  time SMALLINT UNSIGNED NOT NULL,
  cadence_min SMALLINT UNSIGNED NOT NULL,
  cadence_max SMALLINT UNSIGNED NOT NULL,
  interval_min SMALLINT UNSIGNED NOT NULL,
  interval_max SMALLINT UNSIGNED NOT NULL,
  resistance_min TINYINT UNSIGNED NOT NULL,
  resistance_max TINYINT UNSIGNED NOT NULL,
  warm_up_interval SMALLINT UNSIGNED NOT NULL,
  warm_up_cadence SMALLINT UNSIGNED NOT NULL,
  warm_up_resistance TINYINT UNSIGNED NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL REFERENCES users(id)
);
