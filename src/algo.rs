use crate::data::Point;
use crate::timer::TimeMs;

pub fn binary_search<T: From<T>>(data: &Vec<Point<T>>, time: TimeMs) -> &Point<T> {
    let mut high = data.len();
    if high == 0 {
        panic!("Cannot handle empty vec");
    }

    let mut low = 0;
    high -= 1;
    while low != high {
        let current = (high + low).div_ceil(2);
        if data[current].get_time() > time {
            high = current - 1;
        } else {
            low = current;
        }
    }
    &data[high]
}
