use serde::{Deserialize, Serialize};

#[cfg(feature = "diesel")]
use diesel::associations::Identifiable;
#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::Selectable;

#[cfg_attr(feature = "diesel", derive(Queryable, Selectable, Identifiable))]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::users))]
#[derive(Serialize, Deserialize)]
pub struct Users {
    pub id: u64,
    pub name: String,
}
