pub mod algo;
pub mod backend;
pub mod bike;
pub mod bike_error;
pub mod calibration;
pub mod config;
pub mod data;
pub mod device;
mod routes;
pub mod timer;
pub mod training;
pub mod training_data;
pub mod users;
pub mod utils;

#[cfg(feature = "diesel")]
pub mod database;
#[cfg(feature = "diesel")]
pub mod schema;

// android
#[cfg(target_os = "android")]
pub mod mobile;
#[cfg(target_os = "android")]
use std::{thread::sleep, time::Duration};
#[cfg(target_os = "android")]
use tauri;

// tauri
#[cfg(any(feature = "tauri", target_os = "android"))]
pub mod gui;

#[cfg(any(feature = "tauri", target_os = "android"))]
use rocket::{info, tokio};

#[cfg(any(feature = "tauri", target_os = "android"))]
use crate::gui::AppBuilder;

#[cfg(any(feature = "tauri", target_os = "android"))]
pub fn start_with_tauri() {
    info!("Starting app");

    // Start rocket
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.spawn(async { backend::get_rocket().launch().await });

    // Start tauri
    AppBuilder::new().run();
}

#[cfg(target_os = "android")]
#[tauri::mobile_entry_point]
fn android_start() {
    sleep(Duration::new(5, 0));
    start_with_tauri();
}
