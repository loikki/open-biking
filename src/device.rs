use btleplug::api::{
    Central, CentralEvent, Characteristic, Manager as _, Peripheral as _, ScanFilter, WriteType,
};
use btleplug::platform::{Manager, Peripheral};
use futures::StreamExt;
use rocket::tokio::sync::mpsc;
use rocket::{error, info, tokio};
use std::sync::Mutex;
use std::{thread::sleep, time::Duration};
use tokio::sync::mpsc::error::TryRecvError;
use uuid::{uuid, Uuid};

// Own import
use crate::bike_error::BikeError;
use crate::timer::{TimeMs, Timer};
use crate::utils::{self, Cadence, Resistance};

// Bike channel
const NOTIFICATION_CHANNEL: Uuid = uuid!("00001535-1412-efde-1523-785feabcd123");
const WRITE_CHANNEL: Uuid = uuid!("00001534-1412-efde-1523-785feabcd123");
const DEVICE_NAME: &str = "I_EB";

#[derive(Clone, Debug)]
pub struct DeviceData {
    pub time: TimeMs,
    pub resistance: Resistance,
    pub cadence: Cadence,
}

#[derive(Clone, Debug)]
pub enum GetDeviceData {
    All(DeviceData),
    Time(TimeMs),
}

pub enum DeviceCommand {
    Resistance(Resistance),
    StopTimer,
    StartTimer,
    ResetTimer,
    Disconnect,
}

pub struct Device {
    timer: Mutex<Timer>,
    peripheral: Peripheral,
    notification: Characteristic,
    write: Characteristic,
}

impl Device {
    pub async fn new() -> Result<Device, BikeError> {
        let peripheral = Device::find_device().await?;
        Device::connect_to_peripheral(&peripheral).await?;
        let chars = Device::get_characteristics(&peripheral).await?;
        Ok(Device {
            timer: Mutex::new(Timer::new()),
            peripheral,
            notification: chars.0,
            write: chars.1,
        })
    }

    pub async fn is_connected(&self) -> bool {
        self.peripheral.is_connected().await.unwrap_or(false)
    }

    async fn find_device() -> Result<Peripheral, BikeError> {
        info!("Setting up device");
        let manager = Manager::new().await.expect("Failed to get manager");

        // get the first bluetooth adapter
        // connect to the adapter
        info!("Getting adapter");
        let adapters = manager.adapters().await.expect("Failed to get adapter");
        info!("Getting central");
        let central = adapters.into_iter().nth(0).expect("Failed to get central");

        // Each adapter has an event stream, we fetch via events(),
        // simplifying the type, this will return what is essentially a
        // Future<Result<Stream<Item=CentralEvent>>>.
        info!("Getting bluetooth events");
        let mut events = central.events().await.expect("Failed to get events");

        // start scanning for devices
        info!("Starting scan");
        central
            .start_scan(ScanFilter::default())
            .await
            .expect("Failed to start scan");

        // Print based on whatever the event receiver outputs. Note that the event
        // receiver blocks, so in a real program, this should be run in its own
        // thread (not task, as this library does not yet use async channels).
        let mut event_count = 0;
        while let Some(event) = events.next().await {
            event_count += 1;
            if event_count > 10 {
                central.stop_scan().await.expect("Failed to stop scan");
            }
            match event {
                // TODO move everything in this function
                CentralEvent::DeviceDiscovered(id) => {
                    info!("DeviceDiscovered: {:?}", id);
                    let peripheral = central.peripheral(&id).await.expect("Failed to get periph");
                    let name = peripheral
                        .properties()
                        .await
                        .expect("Failed to wait properties")
                        .expect("Failed to get properties")
                        .local_name;
                    if name.is_some() {
                        if name.unwrap() == DEVICE_NAME {
                            info!("Found device");
                            return Ok(central.peripheral(&id).await?);
                        }
                    }
                }
                CentralEvent::DeviceConnected(id) => {
                    info!("Device connected {}", id);
                }
                CentralEvent::DeviceDisconnected(id) => {
                    info!("Device disconnected {}", id);
                }
                _ => {}
            }
        }
        Err(BikeError::PeripheralNotFound(DEVICE_NAME))
    }

    pub async fn connect(&self) -> Result<(), BikeError> {
        Device::connect_to_peripheral(&self.peripheral).await
    }

    async fn connect_to_peripheral(peripheral: &Peripheral) -> Result<(), BikeError> {
        info!("Connecting to peripheral");
        peripheral.connect().await?;
        Ok(())
    }

    pub async fn name(&self) -> String {
        self.peripheral
            .properties()
            .await
            .expect("Failed to get properties")
            .expect("Cannot find name")
            .local_name
            .expect("Did not get a name")
    }

    async fn get_characteristics(
        peripheral: &Peripheral,
    ) -> Result<(Characteristic, Characteristic), BikeError> {
        info!("Getting characteristics");
        peripheral.discover_services().await?;

        // find the characteristic we want
        let chars = peripheral.characteristics();
        let notif_char = chars
            .iter()
            .find(|c| c.uuid == NOTIFICATION_CHANNEL)
            .unwrap();
        let write_char = chars.iter().find(|c| c.uuid == WRITE_CHANNEL).unwrap();
        Ok((notif_char.clone(), write_char.clone()))
    }

    pub async fn subscribe_to_notifications(&self) -> Result<(), BikeError> {
        info!("Asking for notifications");
        if !self.is_connected().await {
            return Err(BikeError::UnexpectedState("Cannot reset"));
        }
        self.peripheral.subscribe(&self.notification).await.unwrap();
        Ok(())
    }

    pub async fn send(&self, data: &[u8]) -> Result<(), BikeError> {
        if !self.is_connected().await {
            info!("Reconnecting");
            self.connect().await?;
        }

        // TODO wait for characteristic changed
        self.peripheral
            .write(&self.write, data, WriteType::WithResponse)
            .await?;
        Ok(())
    }

    pub async fn start_bike(&self) -> Result<(), BikeError> {
        info!("Starting bike");

        let data1 = vec![0xfe, 0x02, 0x08, 0x02];
        let data2 = vec![
            0xff, 0x08, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x81, 0x87, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data3 = vec![
            0xff, 0x08, 0x02, 0x04, 0x02, 0x04, 0x07, 0x04, 0x80, 0x8b, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data4 = vec![
            0xff, 0x08, 0x02, 0x04, 0x02, 0x04, 0x07, 0x04, 0x88, 0x93, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data5 = vec![0xfe, 0x02, 0x0a, 0x02];
        let data6 = vec![
            0xff, 0x0a, 0x02, 0x04, 0x02, 0x06, 0x02, 0x06, 0x82, 0x00, 0x00, 0x8a, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data7 = vec![
            0xff, 0x0a, 0x02, 0x04, 0x02, 0x06, 0x02, 0x06, 0x84, 0x00, 0x00, 0x8c, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data8 = vec![
            0xff, 0x08, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x95, 0x9b, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];
        let data9 = vec![0xfe, 0x02, 0x2c, 0x04];
        let data10 = vec![
            0x00, 0x12, 0x02, 0x04, 0x02, 0x28, 0x07, 0x28, 0x90, 0x07, 0x01, 0xb9, 0xf8, 0x45,
            0x80, 0xc9, 0x10, 0x6d, 0xb8, 0x09,
        ];
        let data11 = vec![
            0x01, 0x12, 0x58, 0xa5, 0xf0, 0x59, 0xa0, 0x1d, 0x78, 0xd9, 0x38, 0x85, 0xe0, 0x49,
            0xd0, 0x2d, 0xb8, 0x09, 0x98, 0xe5,
        ];
        let data12 = vec![
            0xff, 0x08, 0x70, 0xf9, 0x40, 0x98, 0x02, 0x00, 0x00, 0xfc, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];

        let queries = vec![
            &data1, &data2, &data1, &data3, &data1, &data4, &data5, &data6, &data5, &data7, &data1,
            &data8, &data9, &data10, &data11, &data12,
        ];
        let sleep_ms = 400;

        for query in queries {
            self.send(query).await?;
            sleep(Duration::new(0, sleep_ms));
        }

        info!("Init done");
        Ok(())
    }

    async fn update(&self, step: u8, new_resistance: Option<Resistance>) -> Result<(), BikeError> {
        let data = utils::get_update_message(step);
        self.send(&data).await?;

        if step == 6 && new_resistance.is_some() {
            self.send_new_resistance(new_resistance.unwrap()).await?;
        }
        Ok(())
    }

    async fn send_new_resistance(&self, res: Resistance) -> Result<(), BikeError> {
        info!("Updating resistance {}", res);
        if !self.is_connected().await {
            return Err(BikeError::UnexpectedState("Cannot set resistance"));
        }
        let data = utils::get_set_resistance(res);
        self.send(&data).await?;
        Ok(())
    }

    pub async fn notification_loop(&self, sender: mpsc::Sender<GetDeviceData>) {
        info!("Starting read loop");
        let mut notif = self
            .peripheral
            .notifications()
            .await
            .expect("Failed to get notif");
        while let Some(data) = notif.next().await {
            // Don't record data while in pause
            let timer = self.timer.lock().unwrap();
            if !timer.is_running() {
                continue;
            }
            let data = data.value;

            // Filter unwanted messages
            if data.len() != 20
                || data[0] != 0x00
                || data[1] != 0x12
                || data[2] != 0x01
                || data[3] != 0x04
                || (data[12] == 0xFF
                    && data[13] == 0xFF
                    && data[14] == 0xFF
                    && data[15] == 0xFF
                    && data[16] == 0xFF
                    && data[17] == 0xFF
                    && data[18] == 0xFF
                    && data[19] == 0xFF)
            {
                continue;
            }

            // Get power to filter bad messages
            let watts: u16 = (u16::from(data[13]) << 8) + u16::from(data[12]);
            const MAX_WATTS: u16 = 3000;
            if watts > MAX_WATTS {
                continue;
            }

            // Generate new message
            let resistance: Resistance = utils::convert_resistance(data[11]);
            let cadence: Cadence = data[18].into();
            let now = timer.current_ms();

            let send_data = GetDeviceData::All(DeviceData {
                time: now,
                resistance,
                cadence,
            });

            let message = send_data.clone();
            match sender.try_send(send_data) {
                Ok(()) => info!("New data -> {:?}", message),
                Err(x) => info!("Cannot process {:?} due to {}", message, x),
            }
        }
        error!("Leaving read loop");
    }

    pub async fn disconnect(&self) -> Result<(), BikeError> {
        if !self.is_connected().await {
            return Ok(());
        }
        self.peripheral.disconnect().await?;
        Ok(())
    }

    pub async fn update_loop(
        &self,
        receiver: &mut mpsc::Receiver<DeviceCommand>,
        sender: mpsc::Sender<GetDeviceData>,
    ) {
        info!("Starting write loop");
        let mut step: u8 = 0;
        let mut new_resistance: Option<Resistance> = Some(1);
        tokio::time::sleep(tokio::time::Duration::from_millis(1000)).await;
        loop {
            // Update time
            let time = self.timer.lock().unwrap().current_ms();
            sender.send(GetDeviceData::Time(time)).await.unwrap();

            // Get last resistance
            while match receiver.try_recv() {
                Ok(var) => match var {
                    DeviceCommand::Resistance(var) => {
                        new_resistance = Some(var);
                        true
                    }
                    DeviceCommand::StartTimer => self.timer.lock().unwrap().start(),
                    DeviceCommand::StopTimer => self.timer.lock().unwrap().stop(),
                    DeviceCommand::ResetTimer => self.timer.lock().unwrap().reset(),
                    DeviceCommand::Disconnect => {
                        self.peripheral.disconnect().await.unwrap();
                        false
                    }
                },
                Err(TryRecvError::Empty) => false,
                _ => panic!("Unexpected receive"),
            } {}

            // Communicate with bike
            self.update(step, new_resistance)
                .await
                .expect("Update loop failed");

            // Update variables
            step = (step + 1) % 7;
            if step == 0 {
                new_resistance = None;
            } else if step == 6 && new_resistance.is_none() {
                step = 0;
            }

            // Don't spam the bike
            tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
        }
    }
}
