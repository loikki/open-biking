//! Handle the connectivity with the bike
use futures::join;
use rocket::info;
use rocket::tokio::{runtime::Runtime, sync::mpsc};
use std::sync::atomic::{AtomicUsize, Ordering};

use crate::bike_error::BikeError;
use crate::data::{Data, Point};
use crate::device::{Device, DeviceCommand, GetDeviceData};
use crate::timer::TimeMs;
use crate::utils::{restrict_resistance, Resistance};

// Android stuff
#[cfg(target_os = "android")]
use crate::mobile::{setup_class_loader, JAVAVM};
#[cfg(target_os = "android")]
use jni::AttachGuard;
#[cfg(target_os = "android")]
use std::cell::RefCell;
#[cfg(target_os = "android")]
std::thread_local! {
    static JNI_ENV: RefCell<Option<AttachGuard<'static>>> = RefCell::new(None);
}
#[cfg(target_os = "android")]
use crate::mobile::AndroidError;

pub struct Bike {
    runtime: Option<Runtime>,
    receive_channel: Option<mpsc::Receiver<GetDeviceData>>,
    send_channel: Option<mpsc::Sender<DeviceCommand>>,
    data: Data,
    timer_is_running: bool,
    pub delta_res: i8,
}

impl Bike {
    pub fn new() -> Bike {
        Bike {
            runtime: None,
            receive_channel: None,
            send_channel: None,
            data: Data::new(),
            timer_is_running: false,
            delta_res: 0,
        }
    }
    pub async fn connect(&mut self) -> Result<(), BikeError> {
        // Setup runtime
        if self.runtime.is_none() {
            info!("Creating runtime");
            self.runtime = Some(Bike::create_runtime()?);
        }
        let runtime = self.runtime.as_ref().unwrap();

        // Get a device
        let device = runtime.spawn(Device::new()).await??;

        // Init device
        info!("Start bike");

        // Send commands
        let (send1, mut recv1) = mpsc::channel(1);
        self.send_channel = Some(send1);

        // Receive data
        let (send2, recv2) = mpsc::channel(100);
        self.receive_channel = Some(recv2);

        runtime.spawn(async move {
            // Init
            device.subscribe_to_notifications().await.unwrap();
            device.start_bike().await.unwrap();

            // Start loops
            let write = device.update_loop(&mut recv1, send2.clone());
            let read = device.notification_loop(send2);
            join!(read, write);
        });

        Ok(())
    }

    pub async fn fetch_last_data(&mut self) {
        while let Ok(data) = self.receive_channel.as_mut().unwrap().try_recv() {
            match data {
                GetDeviceData::All(data) => {
                    let cadence = Point::new(data.time, data.cadence);
                    let resistance = Point::new(data.time, data.resistance.into());
                    self.data.add(cadence, resistance);
                    self.data.set_time(data.time);
                }
                GetDeviceData::Time(data) => {
                    self.data.set_time(data);
                }
            }
        }
    }

    pub fn is_connected(&self) -> bool {
        // TODO check if device is connected
        self.send_channel.is_some()
            && !self.send_channel.as_ref().unwrap().is_closed()
            && self.receive_channel.is_some()
            && self.runtime.is_some()
    }

    #[cfg(not(target_os = "android"))]
    fn create_runtime() -> Result<Runtime, BikeError> {
        Ok(rocket::tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .enable_all()
            .thread_name_fn(|| {
                static ATOMIC_ID: AtomicUsize = AtomicUsize::new(0);
                let id = ATOMIC_ID.fetch_add(1, Ordering::SeqCst);
                format!("intiface-thread-{}", id)
            })
            .build()
            .unwrap())
    }

    #[cfg(target_os = "android")]
    fn create_runtime() -> Result<Runtime, BikeError> {
        // Give time to accept permissions
        let vm = JAVAVM.get().ok_or(AndroidError::JavaVM)?;
        let env = vm.attach_current_thread().unwrap();

        let class_loader = setup_class_loader(&env);
        Ok(rocket::tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .enable_all()
            .thread_name_fn(|| {
                static ATOMIC_ID: AtomicUsize = AtomicUsize::new(0);
                let id = ATOMIC_ID.fetch_add(1, Ordering::SeqCst);
                format!("intiface-thread-{}", id)
            })
            .on_thread_stop(move || {
                info!("JNI Thread stopped");
                JNI_ENV.with(|f| *f.borrow_mut() = None);
            })
            .on_thread_start(move || {
                info!("JNI Thread started");
                // We now need to call the following code block via JNI calls. God help us.
                //
                //  java.lang.Thread.currentThread().setContextClassLoader(
                //    java.lang.ClassLoader.getSystemClassLoader()
                //  );

                let vm = JAVAVM.get().unwrap();
                let env = vm.attach_current_thread().unwrap();

                let thread = env
                    .call_static_method(
                        "java/lang/Thread",
                        "currentThread",
                        "()Ljava/lang/Thread;",
                        &[],
                    )
                    .unwrap()
                    .l()
                    .unwrap();
                env.call_method(
                    thread,
                    "setContextClassLoader",
                    "(Ljava/lang/ClassLoader;)V",
                    &[class_loader.as_ref().unwrap().as_obj().into()],
                )
                .unwrap();
                JNI_ENV.with(|f| *f.borrow_mut() = Some(env));
            })
            .build()
            .unwrap())
    }

    pub async fn disconnect(&mut self) -> Result<(), BikeError> {
        if !self.is_connected() {
            return Ok(());
        }
        self.send_channel
            .as_ref()
            .unwrap()
            .send(DeviceCommand::Disconnect)
            .await?;
        self.send_channel = None;
        self.receive_channel = None;
        Ok(())
    }

    pub fn update_delta_resistance(&mut self, target: Resistance, requested: Resistance) {
        self.delta_res = requested as i8 - target as i8;
    }

    pub async fn set_resistance(&self, res: Resistance) -> Result<(), BikeError> {
        if !self.is_connected() {
            return Err(BikeError::BikeNotReady("Cannot set resistance"));
        };
        let res = restrict_resistance((res as i8 + self.delta_res) as Resistance);
        self.send_channel
            .as_ref()
            .unwrap()
            .send(DeviceCommand::Resistance(res as Resistance))
            .await?;
        Ok(())
    }

    pub fn is_running(&self) -> bool {
        self.timer_is_running
    }

    pub async fn start(&mut self) -> Result<(), BikeError> {
        self.timer_is_running = true;
        self.send_channel
            .as_ref()
            .unwrap()
            .send(DeviceCommand::StartTimer)
            .await?;
        Ok(())
    }

    pub async fn stop(&mut self) -> Result<(), BikeError> {
        self.timer_is_running = false;
        self.send_channel
            .as_ref()
            .unwrap()
            .send(DeviceCommand::StopTimer)
            .await?;
        Ok(())
    }

    pub async fn reset(&mut self) -> Result<(), BikeError> {
        self.timer_is_running = false;
        self.send_channel
            .as_ref()
            .unwrap()
            .send(DeviceCommand::ResetTimer)
            .await?;
        self.data = Data::new();
        Ok(())
    }

    pub async fn fetch_data(&mut self) -> Data {
        self.fetch_last_data().await;
        self.data.clone()
    }

    pub fn current_time(&self) -> TimeMs {
        self.data.current_time()
    }

    pub fn current_resistance(&self) -> Resistance {
        let time = self.current_time();
        let raw_resistance = self.data.current_resistance(time) as i8;
        let new_resistance = raw_resistance + self.delta_res;
        let res = restrict_resistance(new_resistance as Resistance);
        res as Resistance
    }
}
