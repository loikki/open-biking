use chrono::Local;
use diesel::deserialize::Queryable;
use diesel::mysql::MysqlConnection;
use diesel::prelude::Insertable;
use diesel::result::Error as DieselError;
use diesel::ExpressionMethods;
use diesel::{Connection, Selectable};
use diesel::{QueryDsl, RunQueryDsl, SelectableHelper};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};
use rocket::serde::json::Json;
use serde::Serialize;

use crate::bike_error::BikeError;
use crate::data::Data;
use crate::schema::{training_data, training_settings, users};
use crate::training::Training;
use crate::training_data::TrainingData;
use crate::users::Users;

#[derive(Serialize, Queryable, Selectable, Insertable)]
#[diesel(table_name=crate::schema::users)]
pub struct NewUser {
    pub name: String,
}

pub struct Database {
    sql: MysqlConnection,
}

impl Database {
    pub fn new(url: &String) -> Self {
        Self {
            sql: MysqlConnection::establish(url).unwrap(),
        }
    }

    pub fn migrate(&mut self) {
        // Migrate DB
        const MIGRATIONS: EmbeddedMigrations = embed_migrations!("migrations/");
        self.sql
            .run_pending_migrations(MIGRATIONS)
            .expect("Failed to run migration");
    }

    pub fn get_users(&mut self) -> Vec<Users> {
        users::table
            .select(Users::as_select())
            .load(&mut self.sql)
            .expect("Failed to fetch users")
    }

    pub fn add_user(&mut self, username: &str) -> Result<(), BikeError> {
        let new_user = NewUser {
            name: username.to_string(),
        };
        diesel::insert_into(users::table)
            .values(&new_user)
            .execute(&mut self.sql)?;
        Ok(())
    }

    pub fn update_training_settings(
        &mut self,
        username: &str,
        settings: &Training,
    ) -> Result<(), BikeError> {
        // TODO optimize
        let user = self.get_user(username)?;
        let previous_settings = training_settings::table
            .filter(training_settings::user_id.eq(user.id))
            .select(Training::as_select())
            .load(&mut self.sql)?;

        if previous_settings.len() == 0 {
            diesel::insert_into(training_settings::table)
                .values((training_settings::user_id.eq(user.id), settings))
                .execute(&mut self.sql)?;
        } else {
            diesel::update(training_settings::table)
                .filter(training_settings::user_id.eq(user.id))
                .set(settings)
                .execute(&mut self.sql)?;
        }

        Ok(())
    }

    pub fn get_user(&mut self, username: &str) -> Result<Users, BikeError> {
        let user = users::table
            .select(Users::as_select())
            .filter(users::name.eq(username))
            .first(&mut self.sql)?;
        Ok(user)
    }

    pub fn get_training_settings(&mut self, username: &str) -> Result<Training, BikeError> {
        // TODO optimize
        let user = self.get_user(username)?;

        let settings = match training_settings::table
            .filter(training_settings::user_id.eq(user.id))
            .select(Training::as_select())
            .first(&mut self.sql)
        {
            Ok(settings) => Ok(settings),
            Err(DieselError::NotFound) => Ok(Training::default()),
            Err(x) => Err(x),
        }?;
        Ok(settings)
    }

    pub fn submit_training(&mut self, username: &str, data: &Data) -> Result<(), BikeError> {
        let user = self.get_user(username)?;
        let training_data = TrainingData {
            datetime: Local::now().naive_local(),
            effort: data.get_calories(),
            distance: data.distance(),
            time: data.current_time() as u64,
            average_cadence: data.average_cadence(),
            average_resistance: data.average_resistance(),
            user_id: user.id,
        };

        diesel::insert_into(training_data::table)
            .values(&training_data)
            .execute(&mut self.sql)?;
        Ok(())
    }

    pub fn get_history(&mut self, username: &str) -> Result<Json<Vec<TrainingData>>, BikeError> {
        let user = self.get_user(username)?;
        let history = match training_data::table
            .filter(training_data::user_id.eq(user.id))
            .select(TrainingData::as_select())
            .get_results(&mut self.sql)
        {
            Ok(data) => Ok(data),
            Err(x) => Err(x),
        }?;
        Ok(Json(history))
    }
}
