use rand;
use rand_distr::{Distribution, Normal};
use rocket::serde::{Deserialize, Serialize};

#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::prelude::Insertable;
#[cfg(feature = "diesel")]
use diesel::query_builder::AsChangeset;
#[cfg(feature = "diesel")]
use diesel::Selectable;

use crate::bike_error::BikeError;
use crate::calibration::get_kcalories;
use crate::data::{Data, Point};
use crate::timer::{TimeMin, TimeMs, TimeS, MIN_TO_MS, S_TO_MS};
use crate::utils::{Cadence, KCalories, Resistance};

#[cfg_attr(
    feature = "diesel",
    derive(Queryable, Selectable, AsChangeset, Insertable)
)]
#[cfg_attr(feature="diesel", diesel(table_name = crate::schema::training_settings))]
#[derive(Deserialize, Serialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Training {
    effort: KCalories,
    time: TimeMin,
    cadence_min: Cadence,
    cadence_max: Cadence,
    interval_min: TimeS,
    interval_max: TimeS,
    resistance_min: Resistance,
    resistance_max: Resistance,
    warm_up_interval: TimeS,
    warm_up_cadence: Cadence,
    warm_up_resistance: Resistance,
}

impl Default for Training {
    fn default() -> Self {
        Self {
            effort: 720,
            time: 35,
            cadence_min: 70,
            cadence_max: 110,
            interval_min: 30,
            interval_max: 120,
            resistance_min: 6,
            resistance_max: 11,
            warm_up_interval: 60,
            warm_up_cadence: 60,
            warm_up_resistance: 6,
        }
    }
}

impl Training {
    #[allow(clippy::too_many_arguments)]
    pub fn new(
        effort: KCalories,
        time: TimeMin,
        cadence_min: Cadence,
        cadence_max: Cadence,
        interval_min: TimeS,
        interval_max: TimeS,
        resistance_min: Resistance,
        resistance_max: Resistance,
        warm_up_interval: TimeS,
        warm_up_cadence: Cadence,
        warm_up_resistance: Resistance,
    ) -> Self {
        Training {
            effort,
            time,
            cadence_min,
            cadence_max,
            interval_min,
            interval_max,
            resistance_min,
            resistance_max,
            warm_up_interval,
            warm_up_cadence,
            warm_up_resistance,
        }
    }

    fn get_distribution(min: f32, max: f32) -> Normal<f32> {
        let avg = 0.5 * (min + max);
        let sigma = 0.5 * (max - avg);
        Normal::new(avg, sigma).unwrap()
    }

    fn sample(dist: Normal<f32>, min: f32, max: f32) -> u16 {
        let mut value: f32 = -1.0;
        while value < min || value > max {
            value = dist.sample(&mut rand::thread_rng());
        }
        value as u16
    }

    fn add_warm_up(&self, data: &mut Data) {
        let start = data.duration_ms();
        let end = start + self.warm_up_interval as TimeMs * S_TO_MS;
        data.add(
            Point::new(start, self.warm_up_cadence),
            Point::new(start, self.warm_up_resistance),
        );
        data.add(
            Point::new(end, self.warm_up_cadence),
            Point::new(end, self.warm_up_resistance),
        );
    }

    pub fn generate_training(&self) -> Result<Data, BikeError> {
        let mut data = Data::new();

        // Check if possible
        let min_cal = get_kcalories(
            self.resistance_min,
            self.cadence_min,
            self.time as TimeMs * MIN_TO_MS,
        ) as KCalories;
        let max_cal = get_kcalories(
            self.resistance_max,
            self.cadence_max,
            self.time as TimeMs * MIN_TO_MS,
        ) as KCalories;
        if min_cal > self.effort || max_cal < self.effort {
            return Err(BikeError::InvalidInput(format!(
                "The requested effort cannot be reached {} < {} < {}",
                min_cal, self.effort, max_cal
            )));
        }

        // Get random generators
        let dist_interval =
            Training::get_distribution(self.interval_min.into(), self.interval_max.into());
        let dist_cadence =
            Training::get_distribution(self.cadence_min.into(), self.cadence_max.into());
        let dist_resistance =
            Training::get_distribution(self.resistance_min.into(), self.resistance_max.into());

        let time_ms: TimeMs =
            TimeMs::from(self.time) * MIN_TO_MS - self.warm_up_interval as TimeMs * S_TO_MS;
        let mut last_time: TimeMs = self.warm_up_interval as TimeMs * S_TO_MS;

        // Generate data points
        self.add_warm_up(&mut data);
        while data.duration_ms() < time_ms {
            let cadence_max = if data.len() == 2 {
                (self.cadence_min + self.cadence_max) / 2
            } else {
                self.cadence_max
            };
            let resistance_max = if data.len() == 2 {
                (self.resistance_min + self.resistance_max) / 2
            } else {
                self.resistance_max
            };
            let interval = Training::sample(
                dist_interval,
                self.interval_min.into(),
                self.interval_max.into(),
            ) as TimeMs
                * S_TO_MS;
            let cadence =
                Training::sample(dist_cadence, self.cadence_min.into(), cadence_max.into());
            let resistance = Training::sample(
                dist_resistance,
                self.resistance_min.into(),
                resistance_max.into(),
            );

            data.add(
                Point::new(last_time, cadence),
                Point::new(last_time, resistance as Resistance),
            );
            last_time += interval as TimeMs;
            data.add(
                Point::new(last_time, cadence),
                Point::new(last_time, resistance as Resistance),
            );
        }
        self.add_warm_up(&mut data);

        // Cleanup the training
        let skip = vec![0, 1, data.len() - 2, data.len() - 1];
        self.scale_training(&mut data, &skip);
        data.round_values(&skip);
        Ok(data)
    }

    fn scale_training(&self, data: &mut Data, skip: &Vec<usize>) {
        const MAX_REL_ERROR: f32 = 0.01;
        const MAX_TRIES: u8 = 100;
        let mut tries = 0;
        let mut current_calories = data.get_calories();
        let rel_error = |cal: KCalories| -> f32 { (cal as f32 - self.effort as f32) / cal as f32 };
        while tries < MAX_TRIES && rel_error(current_calories) > MAX_REL_ERROR {
            tries += 1;
            let scale = if current_calories < self.effort {
                1.1
            } else {
                0.9
            };
            data.scale_training(scale, &skip, self.cadence_min, self.cadence_max);
            current_calories = data.get_calories();
        }
    }
}
