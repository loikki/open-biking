use rocket::serde::{Deserialize, Serialize};
use std::cmp;

use crate::algo::binary_search;
use crate::timer::{TimeMs, H_TO_MS};
use crate::utils::restrict_cadence;
use crate::{
    calibration::get_kcalories,
    utils::{Cadence, DistanceKm, KCalories, Resistance},
};

const WHEEL_RATIO: f32 = 0.33;

#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Point<T> {
    x: TimeMs, // Time [ms]
    y: T,      // Cadence / Resistance
}

impl<T> Point<T> {
    pub fn new(x: TimeMs, y: T) -> Self {
        Point { x, y }
    }

    pub fn get_time(&self) -> TimeMs {
        self.x
    }
}

/// Use 2 vectors (time, y) in order to reduce computations
/// on the js side
/// TODO Integrate over surrounding interval not previous one
#[derive(Serialize, Deserialize, Clone)]
#[serde(crate = "rocket::serde")]
pub struct Data {
    cadence: Vec<Point<Cadence>>,
    resistance: Vec<Point<Resistance>>,
    time: TimeMs,
    calories: f32,
    distance: DistanceKm,
}

impl Data {
    pub fn new() -> Self {
        Data {
            cadence: vec![],
            resistance: vec![],
            time: 0,
            calories: 0.0,
            distance: 0.0,
        }
    }

    pub fn reset(&mut self) {
        self.time = 0;
        self.resistance = vec![];
        self.cadence = vec![];
        self.calories = 0.0;
        self.distance = 0.0;
    }

    pub fn add(&mut self, cadence: Point<Cadence>, resistance: Point<Resistance>) {
        let last_cadence = self.cadence.last();
        if last_cadence.is_some() {
            let last_cadence = last_cadence.unwrap();
            let duration = cadence.x - last_cadence.x;
            self.calories += get_kcalories(resistance.y, cadence.y, duration);
            let delta_time = (cadence.x - last_cadence.x) as DistanceKm / H_TO_MS as DistanceKm;
            let speed = WHEEL_RATIO * cadence.y as DistanceKm;
            self.distance += speed * delta_time;
        }

        self.cadence.push(cadence);
        self.resistance.push(resistance);
    }

    pub fn set_time(&mut self, time: TimeMs) {
        self.time = time;
    }

    pub fn duration_ms(&self) -> TimeMs {
        if self.cadence.is_empty() {
            return 0;
        }
        let cad_max = self.cadence.last().unwrap().x;
        let res_max = self.resistance.last().unwrap().x;
        cmp::max(cad_max, res_max)
    }

    pub fn get_calories(&self) -> KCalories {
        self.calories as KCalories
    }

    pub fn round_values(&mut self, skip: &Vec<usize>) {
        for (i, cadence) in self.cadence.iter_mut().enumerate() {
            if skip.contains(&i) {
                continue;
            }
            cadence.y = cadence.y.next_multiple_of(5);
        }
    }

    pub fn len(&self) -> usize {
        self.cadence.len()
    }

    pub fn scale_training(
        &mut self,
        scale: f32,
        skip: &Vec<usize>,
        cadence_min: Cadence,
        cadence_max: Cadence,
    ) {
        self.calories = 0.0;
        for (i, cadence) in self.cadence.iter_mut().enumerate() {
            if skip.contains(&i) {
                continue;
            }
            cadence.y = (cadence.y as f32 * scale) as Cadence;
            cadence.y = restrict_cadence(cadence.y, cadence_min, cadence_max);
            if i == 0 {
                continue;
            }
            let duration = cadence.x - self.resistance[i - 1].x;
            self.calories += get_kcalories(self.resistance[i].y, cadence.y, duration);
        }
    }

    pub fn current_time(&self) -> TimeMs {
        self.time
    }

    pub fn distance(&self) -> f32 {
        self.distance
    }

    pub fn current_resistance(&self, time: TimeMs) -> Resistance {
        if self.resistance.len() == 0 {
            return 1;
        }
        binary_search(&self.resistance, time).y
    }

    pub fn current_cadence(&self, time: TimeMs) -> Cadence {
        if self.resistance.len() == 0 {
            return 1;
        }
        binary_search(&self.cadence, time).y
    }

    fn average<T>(&self, data: &Vec<Point<T>>) -> f32
    where
        f32: From<T>,
        T: Copy,
    {
        let mut avg: f32 = 0.0;
        for (i, value) in data.iter().enumerate() {
            if i == 0 {
                continue;
            }
            let interval: f32 = (value.x - data[i - 1].x) as f32;
            avg += interval * f32::from(value.y);
        }
        avg / self.time as f32
    }

    pub fn average_cadence(&self) -> f32 {
        self.average(&self.cadence)
    }

    pub fn average_resistance(&self) -> f32 {
        self.average(&self.resistance)
    }
}
