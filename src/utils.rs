use std::cmp;

pub type Resistance = u8;
pub type Cadence = u16;
pub type KCalories = u16;
pub type DistanceKm = f32;

pub fn convert_resistance(res: u8) -> Resistance {
    match res {
        0x00..=0x02 => 1,
        0x04 => 2,
        0x07 => 3,
        0x09 => 4,
        0x0b | 0x0c => 5,
        0x0e => 6,
        0x10 | 0x11 => 7,
        0x13 => 8,
        0x15 => 9,
        0x18 => 10,
        0x1a => 11,
        0x1d => 12,
        0x1f => 13,
        0x21 | 0x22 => 14,
        0x24 => 15,
        0x26 | 0x27 => 16,
        _ => panic!("Should not happen {}", res),
    }
}

pub fn get_set_resistance(res: Resistance) -> [u8; 20] {
    match res {
        1 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x32, 0x02, 0x00,
            0x4b, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        2 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xa3, 0x04, 0x00,
            0xbe, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        3 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x14, 0x07, 0x00,
            0x32, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        4 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x85, 0x09, 0x00,
            0xa5, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        5 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xf6, 0x0b, 0x00,
            0x18, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        6 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x67, 0x0e, 0x00,
            0x8c, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        7 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xd8, 0x10, 0x00,
            0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        8 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x49, 0x13, 0x00,
            0x73, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        9 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xba, 0x15, 0x00,
            0xe6, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        10 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x2b, 0x18, 0x00,
            0x5a, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        11 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x9c, 0x1a, 0x00,
            0xcd, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        12 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x0d, 0x1d, 0x00,
            0x41, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        13 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x7e, 0x1f, 0x00,
            0xb4, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        14 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xef, 0x21, 0x00,
            0x27, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        15 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0x60, 0x24, 0x00,
            0x9b, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        16 => [
            0xff, 0x0d, 0x02, 0x04, 0x02, 0x09, 0x07, 0x09, 0x02, 0x01, 0x04, 0xd1, 0x26, 0x00,
            0x0e, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        _ => panic!("Should not happen {}", res),
    }
}

pub fn get_update_message(step: u8) -> Vec<u8> {
    match step {
        0 => vec![0xfe, 0x02, 0x19, 0x03],
        1 => vec![
            0x00, 0x12, 0x02, 0x04, 0x02, 0x15, 0x07, 0x15, 0x02, 0x00, 0x0f, 0x80, 0x00, 0x40,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        2 => vec![
            0xff, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0xfd, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        3 => vec![0xfe, 0x02, 0x17, 0x03],
        4 => vec![
            0x00, 0x12, 0x02, 0x04, 0x02, 0x13, 0x07, 0x13, 0x02, 0x00, 0x0d, 0x3c, 0x9c, 0x31,
            0x00, 0x00, 0x40, 0x40, 0x00, 0x80,
        ],
        5 => vec![
            0xff, 0x05, 0x00, 0x00, 0x00, 0x81, 0xb3, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ],
        6 => vec![0xfe, 0x02, 0x0d, 0x02],
        _ => panic!("Should not happen {}", step),
    }
}

pub const MAX_RES: Resistance = 16;
pub const MIN_RES: Resistance = 1;

pub fn restrict_resistance(res: Resistance) -> Resistance {
    cmp::min(MAX_RES, cmp::max(MIN_RES, res))
}

pub fn restrict_cadence(cadence: Cadence, min: Cadence, max: Cadence) -> Cadence {
    cmp::min(max, cmp::min(min, cadence))
}
