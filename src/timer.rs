use std::time::SystemTime;

pub type TimeMs = u128;
pub type TimeS = u16;
pub type TimeMin = u16;

pub const S_TO_MS: TimeMs = 1000;
pub const MIN_TO_MS: TimeMs = 60 * S_TO_MS;
pub const H_TO_MS: TimeMs = 60 * MIN_TO_MS;

#[derive(Clone)]
pub struct Timer {
    duration_ms: TimeMs,
    time: Option<SystemTime>,
    running: bool,
}

impl Timer {
    pub fn new() -> Self {
        Timer {
            duration_ms: 0,
            time: None,
            running: false,
        }
    }

    pub fn is_running(&self) -> bool {
        self.running
    }

    pub fn start(&mut self) -> bool {
        if self.running {
            return true;
        }
        self.running = true;
        self.time = Some(SystemTime::now());
        true
    }

    pub fn reset(&mut self) -> bool {
        self.duration_ms = 0;
        self.time = None;
        self.running = false;
        true
    }

    pub fn stop(&mut self) -> bool {
        if !self.running {
            return true;
        }

        // Compute the current time
        let delta_ms = self.time.unwrap().elapsed().unwrap().as_millis();
        self.duration_ms += delta_ms;

        self.running = false;
        self.time = Some(SystemTime::now());
        true
    }

    pub fn current_ms(&self) -> TimeMs {
        if !self.running {
            return self.duration_ms;
        }
        let delta_ms = self.time.unwrap().elapsed().unwrap().as_millis();
        self.duration_ms + delta_ms
    }
}

#[cfg(test)]
mod tests {
    use crate::timer::{TimeMs, Timer};
    use std::time::{SystemTime, UNIX_EPOCH};

    impl Timer {
        pub fn test_new(duration_ms: TimeMs, time: SystemTime) -> Self {
            Timer {
                duration_ms,
                time: Some(time),
                running: false,
            }
        }

        pub fn set_running(&mut self, running: bool) {
            self.running = running;
        }
    }

    #[test]
    fn test_running() {
        let mut timer = Timer::test_new(100, UNIX_EPOCH);
        assert_eq!(timer.current_ms(), 100);

        timer.set_running(true);
        let total = UNIX_EPOCH.elapsed().unwrap().as_millis();
        let current = timer.current_ms();
        assert!(current > total);
        assert!(current < total + 1000);
    }
}
