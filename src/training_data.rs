use chrono::NaiveDateTime;
use rocket::serde::{Deserialize, Serialize};

use crate::utils::KCalories;

#[cfg(feature = "diesel")]
use diesel::deserialize::Queryable;
#[cfg(feature = "diesel")]
use diesel::prelude::Insertable;
#[cfg(feature = "diesel")]
use diesel::Selectable;

#[cfg_attr(feature = "diesel", derive(Selectable, Queryable, Insertable))]
#[cfg_attr(feature="diesel", diesel(table_name=crate::schema::training_data))]
#[derive(Deserialize, Serialize)]
pub struct TrainingData {
    pub datetime: NaiveDateTime,
    pub effort: KCalories,
    pub distance: f32,
    pub time: u64,
    pub average_cadence: f32,
    pub average_resistance: f32,
    pub user_id: u64,
}
