use btleplug::Error as BtleplugError;
#[cfg(feature = "diesel")]
use diesel::result::Error as DieselError;
use rocket::http::Status;
use rocket::response::Responder;
use rocket::tokio::{sync::mpsc::error::SendError, task::JoinError};
use rocket::{response, Request};
use thiserror::Error;

use crate::device::DeviceCommand;
#[cfg(target_os = "android")]
use crate::mobile::AndroidError;

#[derive(Debug, Error)]
pub enum BikeError {
    #[error("Openbiking error: {0}")]
    OpenBiking(String),

    #[error("Btleplug error: {0}")]
    Btleplug(#[from] BtleplugError),

    #[cfg(target_os = "android")]
    #[error("Android error: {0}")]
    Android(#[from] AndroidError),

    #[error("Join error: {0}")]
    Join(#[from] JoinError),

    // TODO use openbiking
    #[error("Peripheral not found {0}")]
    PeripheralNotFound(&'static str),

    // TODO use openbiking
    #[error("Unexpected state for the bike {0}")]
    UnexpectedState(&'static str),

    // TODO use openbiking
    #[error("Bike not ready: {0}")]
    BikeNotReady(&'static str),

    #[error("Send error: {0}")]
    Send(#[from] SendError<DeviceCommand>),

    // TODO use openbiking
    #[error("Invalid input: {0}")]
    InvalidInput(String),

    #[error("Request failed: {0}")]
    Reqwest(#[from] reqwest::Error),

    #[cfg(feature = "diesel")]
    #[error("SQL error: {0}")]
    Diesel(#[from] DieselError),
}

impl<'r> Responder<'r, 'static> for BikeError {
    fn respond_to(self, req: &'r Request<'_>) -> response::Result<'static> {
        let string = format!("{}", self);
        let status = match self {
            Self::PeripheralNotFound(_) => Status::NotFound,
            Self::InvalidInput(_) => Status::BadRequest,
            _ => Status::InternalServerError,
        };
        response::Response::build_from(string.respond_to(req)?)
            .status(status)
            .ok()
    }
}
