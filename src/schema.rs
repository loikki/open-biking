// @generated automatically by Diesel CLI.

diesel::table! {
    training_data (id) {
        id -> Unsigned<Bigint>,
        datetime -> Datetime,
        effort -> Unsigned<Smallint>,
        distance -> Float,
        time -> Unsigned<Bigint>,
        average_cadence -> Float,
        average_resistance -> Float,
        user_id -> Unsigned<Bigint>,
    }
}

diesel::table! {
    training_settings (id) {
        id -> Unsigned<Bigint>,
        effort -> Unsigned<Smallint>,
        time -> Unsigned<Smallint>,
        cadence_min -> Unsigned<Smallint>,
        cadence_max -> Unsigned<Smallint>,
        interval_min -> Unsigned<Smallint>,
        interval_max -> Unsigned<Smallint>,
        resistance_min -> Unsigned<Tinyint>,
        resistance_max -> Unsigned<Tinyint>,
        warm_up_interval -> Unsigned<Smallint>,
        warm_up_cadence -> Unsigned<Smallint>,
        warm_up_resistance -> Unsigned<Tinyint>,
        user_id -> Unsigned<Bigint>,
    }
}

diesel::table! {
    users (id) {
        id -> Unsigned<Bigint>,
        #[max_length = 255]
        name -> Varchar,
    }
}

diesel::allow_tables_to_appear_in_same_query!(training_data, training_settings, users,);
