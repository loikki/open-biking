use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{get, http::Status, post, routes, Route, State};

use crate::bike_error::BikeError;
use crate::config::Config;
use crate::data::Data;
use crate::training::Training;

use reqwest::StatusCode;

#[cfg(feature = "diesel")]
use crate::database::Database;

pub fn get_routes() -> Vec<Route> {
    routes![get_training, generate_training, get_training_settings]
}

#[cfg(not(feature = "diesel"))]
use reqwest;

#[get("/")]
async fn get_training(state: &State<Mutex<Data>>) -> Json<Data> {
    let training = state.inner().lock().await;
    Json(training.clone())
}

#[cfg(feature = "diesel")]
async fn update_db(username: &str, settings: &Training, config: &Config) -> Result<(), BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        db.update_training_settings(username, &settings)?;
        Ok(())
    } else {
        send_to_server(username, settings, config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn update_db(username: &str, settings: &Training, config: &Config) -> Result<(), BikeError> {
    send_to_server(username, settings, config).await
}

async fn send_to_server(
    username: &str,
    settings: &Training,
    config: &Config,
) -> Result<(), BikeError> {
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let client = reqwest::Client::new();
        let resp = client
            .post(format!("{url}/training/{username}"))
            .json(&settings)
            .send()
            .await?;
        if resp.status() != StatusCode::OK {
            let text = resp.text().await?;
            return Err(BikeError::OpenBiking(text));
        }
    }
    Ok(())
}

#[post("/<username>", data = "<settings>")]
async fn generate_training(
    username: &str,
    training: &State<Mutex<Data>>,
    config: &State<Mutex<Config>>,
    settings: Json<Training>,
) -> Result<(), BikeError> {
    let settings = settings.into_inner();
    let config = config.inner().lock().await;

    // Update DB
    update_db(username, &settings, &config).await?;

    // Generate training
    let data = settings.generate_training()?;
    let mut training = training.inner().lock().await;
    *training = data;
    Ok(())
}

#[cfg(feature = "diesel")]
async fn get_training_settings_internal(
    username: &str,
    config: &Config,
) -> Result<(Status, Json<Training>), BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        let training = db.get_training_settings(username)?;
        Ok((Status::Ok, Json(training)))
    } else {
        get_training_settings_from_server(username, config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn get_training_settings_internal(
    username: &str,
    config: &Config,
) -> Result<(Status, Json<Training>), BikeError> {
    get_training_settings_from_server(username, config).await
}

async fn get_training_settings_from_server(
    username: &str,
    config: &Config,
) -> Result<(Status, Json<Training>), BikeError> {
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let resp = reqwest::get(format!("{url}/training/{username}")).await?;
        if resp.status() != StatusCode::OK {
            let text = resp.text().await?;
            return Err(BikeError::OpenBiking(text));
        }
        // TODO avoid multiple conversion
        let output: Training = resp.json().await?;
        Ok((Status::Ok, Json(output)))
    } else {
        Ok((Status::Ok, Json(Training::default())))
    }
}

#[get("/<username>")]
async fn get_training_settings(
    username: &str,
    config: &State<Mutex<Config>>,
) -> Result<(Status, Json<Training>), BikeError> {
    let config = config.inner().lock().await;
    get_training_settings_internal(username, &config).await
}
