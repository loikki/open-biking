use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{get, http::Status, post, routes, Route, State};

use crate::bike::Bike;
use crate::bike_error::BikeError;
use crate::data::Data;
use crate::utils::{Resistance, MAX_RES, MIN_RES};

pub fn get_routes() -> Vec<Route> {
    routes![get_bike, set_resistance, connect, status, disconnect]
}

#[get("/")]
async fn get_bike(state_bike: &State<Mutex<Bike>>, state_data: &State<Mutex<Data>>) -> Json<Data> {
    let mut bike = state_bike.inner().lock().await;
    if !bike.is_connected() {
        return Json(Data::new());
    }
    // Ensure that the resistance is correct
    let time = bike.current_time();
    let target = state_data.inner().lock().await.current_resistance(time);
    if target != bike.current_resistance() {
        bike.set_resistance(target).await.unwrap();
    }

    Json(bike.fetch_data().await)
}

#[post("/resistance/<resistance>")]
async fn set_resistance(
    state_bike: &State<Mutex<Bike>>,
    state_data: &State<Mutex<Data>>,
    resistance: Resistance,
) -> Result<Status, BikeError> {
    if !(MIN_RES..=MAX_RES).contains(&resistance) {
        return Ok(Status::BadRequest);
    }

    // Get current time
    let mut bike = state_bike.inner().lock().await;
    let time = bike.current_time();

    // Update the delta
    let target = state_data.inner().lock().await.current_resistance(time);
    bike.update_delta_resistance(target, resistance);

    // Update resistance
    bike.set_resistance(target).await.unwrap();
    Ok(Status::Ok)
}

#[get("/connect")]
async fn connect(state: &State<Mutex<Bike>>) -> Result<Status, BikeError> {
    let mut bike = state.inner().lock().await;
    bike.connect().await?;
    Ok(Status::Ok)
}

#[get("/status")]
async fn status(state: &State<Mutex<Bike>>) -> Result<(Status, &'static str), BikeError> {
    let bike = state.inner().lock().await;
    if !bike.is_connected() {
        return Ok((Status::Ok, "disconnected"));
    }
    Ok((Status::Ok, "connected"))
}

#[get("/disconnect")]
async fn disconnect(state: &State<Mutex<Bike>>) -> Result<Status, BikeError> {
    let mut bike = state.inner().lock().await;
    if !bike.is_connected() {
        return Ok(Status::Ok);
    }
    bike.disconnect().await?;
    Ok(Status::Ok)
}
