use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{delete, get, http::Status, post, routes, Route, State};

use crate::bike::Bike;
use crate::bike_error::BikeError;
use crate::config::Config;
use crate::data::Data;
use crate::training_data::TrainingData;
use crate::utils::Cadence;

use reqwest::StatusCode;

pub fn get_routes() -> Vec<Route> {
    routes![
        stop,
        start,
        is_running,
        reset,
        get_target,
        submit,
        get_history,
        set_backend,
        get_backend,
        delete_backend
    ]
}

#[cfg(feature = "diesel")]
use crate::database::Database;

#[cfg(not(feature = "diesel"))]
use reqwest;

#[post("/stop")]
async fn stop(state: &State<Mutex<Bike>>) -> Status {
    let mut bike = state.inner().lock().await;
    if !bike.is_connected() || !bike.is_running() {
        return Status::ServiceUnavailable;
    }
    bike.stop().await.unwrap();
    Status::Ok
}

#[post("/start")]
async fn start(state: &State<Mutex<Bike>>) -> Status {
    let mut bike = state.inner().lock().await;
    if !bike.is_connected() || bike.is_running() {
        return Status::ServiceUnavailable;
    }
    bike.start().await.unwrap();
    Status::Ok
}

#[get("/running")]
async fn is_running(state: &State<Mutex<Bike>>) -> (Status, &'static str) {
    let bike = state.inner().lock().await;
    if !bike.is_connected() {
        return (Status::Ok, "stopped");
    }
    let running = bike.is_running();
    (Status::Ok, if running { "running" } else { "stopped" })
}

#[post("/reset")]
async fn reset(state_bike: &State<Mutex<Bike>>, state_data: &State<Mutex<Data>>) -> Status {
    let mut bike = state_bike.inner().lock().await;
    if !bike.is_connected() {
        return Status::ServiceUnavailable;
    }
    bike.reset().await.unwrap();
    state_data.inner().lock().await.reset();
    Status::Ok
}

#[get("/target")]
async fn get_target(
    state_bike: &State<Mutex<Bike>>,
    state_data: &State<Mutex<Data>>,
) -> Json<Cadence> {
    let bike = state_bike.inner().lock().await;
    if !bike.is_connected() {
        return Json(0);
    }
    // Ensure that the resistance is correct
    let time = bike.current_time();
    let target = state_data.inner().lock().await.current_cadence(time);
    Json(target)
}

#[cfg(feature = "diesel")]
async fn submit_internal(username: &str, data: &Data, config: &Config) -> Result<(), BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        db.submit_training(username, &data)?;
        return Ok(());
    } else {
        submit_to_server(username, &data, &config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn submit_internal(username: &str, data: &Data, config: &Config) -> Result<(), BikeError> {
    submit_to_server(username, &data, &config).await
}

async fn submit_to_server(username: &str, data: &Data, config: &Config) -> Result<(), BikeError> {
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let client = reqwest::Client::new();
        let resp = client
            .post(format!("{url}/submit/{username}"))
            .json(data)
            .send()
            .await?;
        if resp.status() == StatusCode::OK {
            return Ok(());
        }
        let text = resp.text().await?;
        Err(BikeError::OpenBiking(text))
    } else {
        Ok(())
    }
}

#[post("/submit/<username>", data = "<data>")]
async fn submit(
    username: &str,
    data: Option<Json<Data>>,
    bike: &State<Mutex<Bike>>,
    config: &State<Mutex<Config>>,
) -> Result<(), BikeError> {
    let config = config.inner().lock().await;
    let inner_data: Data = if data.is_none() {
        bike.inner().lock().await.fetch_data().await
    } else {
        data.unwrap().into_inner()
    };

    submit_internal(username, &inner_data, &config).await
}

#[cfg(feature = "diesel")]
async fn get_history_internal(
    username: &str,
    config: &Config,
) -> Result<Json<Vec<TrainingData>>, BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        let history = db.get_history(username)?;
        return Ok(history);
    } else {
        get_history_from_server(username, &config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn get_history_internal(
    username: &str,
    config: &Config,
) -> Result<Json<Vec<TrainingData>>, BikeError> {
    get_history_from_server(username, &config).await
}

async fn get_history_from_server(
    username: &str,
    config: &Config,
) -> Result<Json<Vec<TrainingData>>, BikeError> {
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let client = reqwest::Client::new();
        let resp = client
            .get(format!("{url}/history/{username}"))
            .send()
            .await?;
        if resp.status() != StatusCode::OK {
            let text = resp.text().await?;
            return Err(BikeError::OpenBiking(text));
        }
        let output: Vec<TrainingData> = resp.json().await?;
        Ok(Json(output))
    } else {
        Ok(Json(vec![]))
    }
}

#[get("/history/<username>")]
async fn get_history(
    username: &str,
    config: &State<Mutex<Config>>,
) -> Result<Json<Vec<TrainingData>>, BikeError> {
    let config = config.inner().lock().await;
    get_history_internal(username, &config).await
}

#[post("/backend/<url>")]
async fn set_backend(url: &str, config: &State<Mutex<Config>>) {
    let mut config = config.inner().lock().await;
    config.set_backend_url(url)
}

#[delete("/backend")]
async fn delete_backend(config: &State<Mutex<Config>>) {
    let mut config = config.inner().lock().await;
    config.delete_backend_url();
}

#[get("/backend")]
async fn get_backend(config: &State<Mutex<Config>>) -> (Status, String) {
    let config = config.inner().lock().await;
    let url = config.backend_url();
    if url.is_some() {
        (Status::Ok, url.as_ref().unwrap().to_string())
    } else {
        (Status::NotFound, "Backend not set".to_string())
    }
}
