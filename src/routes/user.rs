use rocket::http::Status;
use rocket::serde::json::Json;
use rocket::tokio::sync::Mutex;
use rocket::{get, post, routes, Route, State};

use crate::bike_error::BikeError;
use crate::config::Config;
use crate::users::Users;

use reqwest::StatusCode;

#[cfg(feature = "diesel")]
use crate::database::Database;

pub fn get_routes() -> Vec<Route> {
    routes![get_users, add_user,]
}

#[cfg(not(feature = "diesel"))]
use reqwest;

#[cfg(feature = "diesel")]
async fn get_users_internal(config: &Config) -> Result<Json<Vec<Users>>, BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        Ok(Json(db.get_users()))
    } else {
        get_users_from_server(config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn get_users_internal(config: &Config) -> Result<Json<Vec<Users>>, BikeError> {
    get_users_from_server(config).await
}

async fn get_users_from_server(config: &Config) -> Result<Json<Vec<Users>>, BikeError> {
    // Send data to backend
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let resp = reqwest::get(format!("{url}/users")).await?;
        if resp.status() != StatusCode::OK {
            return Err(BikeError::OpenBiking("Failed to fetch users".to_string()));
        }
        let output: Vec<Users> = resp.json().await?;
        Ok(Json(output))
    }
    // Provide default values
    else {
        let users = vec![Users {
            id: 0,
            name: String::from("Default"),
        }];
        Ok(Json(users))
    }
}

#[get("/")]
async fn get_users(config: &State<Mutex<Config>>) -> Result<Json<Vec<Users>>, BikeError> {
    let config = config.inner().lock().await;
    get_users_internal(&config).await
}

#[cfg(feature = "diesel")]
async fn add_user_internal(username: &str, config: &Config) -> Result<Status, BikeError> {
    if config.backend_url().is_none() {
        let mut db = Database::new(config.database_url());
        db.add_user(&username)?;
        Ok(Status::Ok)
    } else {
        add_user_to_server(username, config).await
    }
}

#[cfg(not(feature = "diesel"))]
async fn add_user_internal(username: &str, config: &Config) -> Result<Status, BikeError> {
    add_user_to_server(username, config).await
}

async fn add_user_to_server(username: &str, config: &Config) -> Result<Status, BikeError> {
    // Send data to backend
    if config.backend_url().is_some() {
        let url = config.backend_url().as_ref().unwrap();
        let client = reqwest::Client::new();
        let resp = client
            .post(format!("{url}/users/{username}"))
            .send()
            .await?;
        if resp.status() == StatusCode::OK {
            return Ok(Status::Ok);
        }
        Err(BikeError::OpenBiking("Failed to add username".to_string()))
    } else {
        Ok(Status::Ok)
    }
}

#[post("/<username>")]
async fn add_user(username: &str, config: &State<Mutex<Config>>) -> Result<Status, BikeError> {
    let config = config.inner().lock().await;
    add_user_internal(username, &config).await
}
