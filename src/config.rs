use dotenv::dotenv;
use rocket::{error, info};
use std::env;

pub struct Config {
    database_url: String,
    backend_url: Option<String>,
    expose_rocket: bool,
    frontend_path: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            database_url: "not-set".to_string(),
            backend_url: None,
            expose_rocket: false,
            frontend_path: "frontend/build".to_string(),
        }
    }
}

impl Config {
    pub fn new() -> Self {
        match dotenv().ok() {
            Some(_) => info!("Reading environment for dotenv"),
            None => error!("Cannot read environment from dotenv"),
        }

        let database_url = Config::read_string_variable("DATABASE_URL", "not-set");
        let frontend_path = Config::read_string_variable("FRONTEND_PATH", "frontend/build");

        let expose_rocket = Config::read_bool_variable("EXPOSE_ROCKET", false);
        let backend_url = Config::read_optional_string_variable("BACKEND_URL");

        Self {
            database_url,
            backend_url,
            expose_rocket,
            frontend_path,
        }
    }

    pub fn read_optional_string_variable(var: &str) -> Option<String> {
        match env::var(var) {
            Ok(val) => {
                info!("Found {}", var);
                Some(val)
            }
            Err(_) => None,
        }
    }

    pub fn read_string_variable(var: &str, default: &str) -> String {
        match env::var(var) {
            Ok(val) => {
                info!("Found {}", var);
                val
            }
            Err(_) => default.to_string(),
        }
    }

    pub fn read_bool_variable(var: &str, default: bool) -> bool {
        match env::var(var) {
            Ok(val) => {
                let val = match val.as_str() {
                    "yes" | "1" | "true" | "True" => true,
                    _ => false,
                };
                info!("Found {}: {}", var, val);
                val
            }
            Err(_) => default,
        }
    }

    pub fn set_backend_url(&mut self, url: &str) {
        self.backend_url = Some(String::from(url));
    }

    pub fn delete_backend_url(&mut self) {
        self.backend_url = None;
    }

    pub fn backend_url(&self) -> &Option<String> {
        &self.backend_url
    }

    pub fn expose_rocket(&self) -> bool {
        self.expose_rocket
    }

    pub fn database_url(&self) -> &String {
        &self.database_url
    }

    pub fn frontend_path(&self) -> &String {
        &self.frontend_path
    }
}
