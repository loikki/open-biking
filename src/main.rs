// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

// With tauri
#[cfg(feature = "tauri")]
use open_biking_lib::start_with_tauri;

#[cfg(feature = "tauri")]
pub fn main() {
    start_with_tauri();
}

// Without tauri
#[cfg(feature = "diesel")]
use rocket::{fs::FileServer, launch};

#[cfg(feature = "diesel")]
use open_biking_lib::backend;

#[cfg(feature = "diesel")]
#[launch]
fn rocket() -> _ {
    use open_biking_lib::config::Config;
    let config = Config::new();
    backend::get_rocket().mount("/", FileServer::from(config.frontend_path()))
}
