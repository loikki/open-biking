use std::env;

fn main() {
    // Tauri feature does not work properly => use derived variable
    match env::var("CARGO_FEATURE_TAURI") {
        Ok(_) => tauri_build::build(),
        _ => {}
    }
}
