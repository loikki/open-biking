# Open Biking

Bluetooth application for generating training on an indoor bike

This application allows you to connect your indoor bike (CBC Pro-form [could be extended to other bikes]) to your phone and automatically generate a random training. This application requires a database and the server running on another device.

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="80">](https://f-droid.org/packages/io.open_biking/)

Or download the APK from the [Releases Section](https://gitlab.com/loikki1/open-biking/-/releases).