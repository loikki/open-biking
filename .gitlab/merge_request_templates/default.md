* [] Create a tag (e.g. 1.0.0)
* [] Update tauri cli in `versions`
* [] Write a changelog in `fastlane`
* [] Update version in `android-files/AndroidManifest.xml`
* [] Update version in `android-files/build.gradle.kts`
* [] Update version in `Cargo.toml`
