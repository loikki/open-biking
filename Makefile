.ONESHELL:
SHELL = /bin/bash
ANDROID_NATIVE_API_LEVEL = 25
NDK_VERSION ?= 25.1.8937393
NDK_HOME ?= /opt/android-sdk/ndk/$(NDK_VERSION)
ANDROID_OPTIONS ?= --features tauri
DESKTOP_OPTIONS ?= --features tauri
SERVER_OPTIONS ?= --features diesel
SQL_PASSWORD ?= 1234
SQL_DB_NAME ?= open-biking
SQL_USER ?= me
RANLIB ?= $(NDK_HOME)/toolchains/llvm/prebuilt/linux-x86_64/bin/llvm-ranlib
ARGS ?=

init:
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android init
	echo "Don't foget to set your jvm version in ~/.gradle/gradle.properties (/usr/lib/jvm/...)"
	cargo tauri icon icon.png

# Dev
run-pipeline:
	gitlab-runner exec docker $(ARGS)

run:
	cargo tauri dev $(DESKTOP_OPTIONS)

update:
	cargo update

doc:
	cargo doc --open

outdated-dependencies:
	cargo outdated

format:
	cargo fmt
	cd frontend
	npm run format

check-format:
	cargo fmt --all -- --check
	cargo clippy

test:
	cargo test

# Frontend
frontend-install-dep:
	cd frontend
	npm install

frontend-build:
	cd frontend
	npm run build

frontend-run:
	cd frontend
	npm run dev

# Desktop
desktop-build-debug:
	cargo tauri build $(DESKTOP_OPTIONS)

desktop-build:
	cargo tauri build $(DESKTOP_OPTIONS)

desktop-test-ubuntu:
	docker run --net=host --env DISPLAY=$$DISPLAY --device /dev/kfd --device /dev/dri --group-add video -v $$PWD:$$PWD ubuntu:22.04 /bin/bash -c "dpkg -i $$PWD/target/x86_64-unknown-linux-gnu/release/bundle/deb/*_amd64.deb || apt update && apt install -y -f && open-biking"

# Server
server-build:
	cargo build --release $(SERVER_OPTIONS)

server-run:
	cargo run $(SERVER_OPTIONS)


# Android
android-dev: .android-prepare
	adb uninstall io.gitlab.open_biking | true
	export NDK_HOME=$(NDK_HOME)
	export PKG_CONFIG_PATH=/usr/lib/x86_64-linux-gnu/pkgconfig/:$PKG_CONFIG_PATH
	export PKG_CONFIG_SYSROOT_DIR=/
	cargo tauri android dev $(ANDROID_OPTIONS)

.android-prepare:
	source .password
	./copy-android

android-build: .android-prepare
	export RANLIB=$(RANLIB)
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android build --apk $(ANDROID_OPTIONS)
	cp gen/android/app/build/outputs/apk/universal/release/app-universal-release.apk open-biking.apk

android-build-debug: .android-prepare
	export NDK_HOME=$(NDK_HOME)
	cargo tauri android build $(ANDROID_OPTIONS) --debug
	cp gen/android/app/build/outputs/apk/universal/debug/app-universal-debug.apk open-biking.apk

android-sign:
	/opt/android-sdk/build-tools/*/apksigner sign --ks ~/android.keystore open-biking.apk

android-generate-key:
	keytool -genkey -v -keystore upload-keystore.jks -keyalg RSA -keysize 2048 -validity 10000 -alias upload

android-deploy:
	adb uninstall io.gitlab.open_biking | true
	adb install open-biking.apk

android-logs:
	pid=`adb shell pidof io.gitlab.open_biking`
	adb logcat --pid=$$pid
	echo $$pid


android-logs-clear:
	adb logcat -c

android-install-sdk:
	sudo sdkmanager --install "ndk;$(NDK_VERSION)"
	sudo sdkmanager --install "build-tools;33.0.0"
	sudo sdkmanager --install "platforms;android-$(ANDROID_NATIVE_API_LEVEL)"
	sudo sdkmanager --install "platform-tools"
	sudo sdkmanager --licenses

android-add-target:
	rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android x86_64-linux-android


# DB
db-start:
	docker run --network host --name mysql -e MYSQL_ROOT_PASSWORD=$(SQL_PASSWORD) -e MYSQL_DATABASE=$(SQL_DB_NAME) -e MYSQL_USER=$(SQL_USER) -e MYSQL_PASSWORD=$(SQL_PASSWORD) -d mysql:latest

db-write-env:
	echo DATABASE_URL=mysql://$(SQL_USER):$(SQL_PASSWORD)@127.0.0.1:3306/$(SQL_DB_NAME) > .env

db-stop:
	docker container rm -f mysql

db-logs:
	docker logs mysql

db-migrate:
	diesel migration run
